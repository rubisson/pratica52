/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
import utfpr.ct.dainf.if62c.pratica.Equacao2grau;

public class Pratica52 {
    public static void main(String[] args) {
        Integer a,b,c;
        a=1;b=2;c=1;
        try{
            Equacao2grau<Integer> eq = new Equacao2grau<>(a,b,c);
            System.out.println("A raiz 01 é: " + eq.getRaiz1());
            System.out.println("A raiz 02 é: " + eq.getRaiz2());
        }catch(RuntimeException rex){
            System.out.println(rex.getMessage());
        }

    }
}
